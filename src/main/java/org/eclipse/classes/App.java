package org.eclipse.classes;

import java.lang.reflect.Array;
import java.util.Date;
import java.util.Arrays;

import org.eclipse.models.Employee;
import org.eclipse.models.User;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Hello world!
 *
 */
public class App {
	public static void main( String[] args )
    {
        System.out.println( "-------------------------------------------" );
        
        User u = new User(5, "albert", "Siko", "alberto@gmail.com");
        
        Gson gson = new Gson();
        
       String jsonString = gson.toJson(u);
       
       System.out.println(jsonString);
       
       System.out.println( "---------------------------------------------" );
       
        String jsonString2 = "{\"id\":5,\"firstName\":\"albert\",\"lastName\":\"Siko\",\"email\":\"alberto@gmail.com\"}";
        
        		User userObject = gson.fromJson(jsonString2, User.class);
        System.out.println(userObject);
        
        System.out.println( "---------------------------------------------" );
        
        Employee employee = new Employee();
        employee.setId(1);
        employee.setFirstName("Bruce");
        employee.setLastName("Wayne");
        employee.setRoles(Arrays.asList("ADMIN", "Manager"));
        
        
     String employeeJson = gson.toJson(employee);
     System.out.println(employeeJson);
     
     System.out.println( "---------------------------------------------" );
     
     Employee employee2 = new Employee();
     employee2.setId(1);
     employee2.setFirstName("Bruce");
     employee2.setLastName("Wayne");
     employee2.setBirthDate(new Date("1985/10/22"));
     employee2.setRoles(Arrays.asList("ADMIN", "Manager"));
     
     GsonBuilder gsonBuilder = new GsonBuilder();
     gsonBuilder.registerTypeAdapter(Date.class, new DateSerializer()); 
     Gson gson1 = gsonBuilder.create();
     
     System.out.println(gson1.toJson(employee2));
     
     //System.out.println(gson1.fromJson("{\"id\":1,\"firstName\":\"Bruce\",\"lastName\":\"Wayne\",\"birthDate\":\"22/10/1985\",\"roles\":[\"ADMIN\",\"Manager\"]}", Employee.class));
    }
}
